/*
 * Copyright (c) 2013 Samsung Electronics Co., Ltd.
 *		http://www.samsung.com
 *
 * Samsung EXYNOS5 SoC series SCALER driver
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#ifndef REGS_SCALER_H_
#define REGS_SCALER_H_

#include "scaler.h"

/* SCALER status */
#define SCALER_STATUS				0x00
#define SCALER_STATUS_RUNNING			(1 << 1)
#define SCALER_STATUS_READY_CLK_DOWN		(1 << 0)

/* SCALER config */
#define SCALER_CFG				0x04
#define SCALER_CFG_FILL_EN			(1 << 24)
#define SCALER_CFG_BLEND_CLR_DIV_ALPHA_EN	(1 << 17)
#define SCALER_CFG_BLEND_EN			(1 << 16)
#define SCALER_CFG_CSC_Y_OFFSET_SRC_EN		(1 << 10)
#define SCALER_CFG_CSC_Y_OFFSET_DST_EN		(1 << 9)
#define SCALER_CFG_16_BURST_MODE		(1 << 8)
#define SCALER_CFG_SOFT_RESET			(1 << 1)
#define SCALER_CFG_START_CMD			(1 << 0)

/* SCALER interrupts */
#define SCALER_INT_TIMEOUT			31
#define SCALER_INT_ILLEGAL_BLEND		24
#define SCALER_INT_ILLEGAL_RATIO		23
#define SCALER_INT_ILLEGAL_DST_HEIGHT		22
#define SCALER_INT_ILLEGAL_DST_WIDTH		21
#define SCALER_INT_ILLEGAL_DST_V_POS		20
#define SCALER_INT_ILLEGAL_DST_H_POS		19
#define SCALER_INT_ILLEGAL_DST_C_SPAN		18
#define SCALER_INT_ILLEGAL_DST_Y_SPAN		17
#define SCALER_INT_ILLEGAL_DST_CR_BASE		16
#define SCALER_INT_ILLEGAL_DST_CB_BASE		15
#define SCALER_INT_ILLEGAL_DST_Y_BASE		14
#define SCALER_INT_ILLEGAL_DST_COLOR		13
#define SCALER_INT_ILLEGAL_SRC_HEIGHT		12
#define SCALER_INT_ILLEGAL_SRC_WIDTH		11
#define SCALER_INT_ILLEGAL_SRC_CV_POS		10
#define SCALER_INT_ILLEGAL_SRC_CH_POS		9
#define SCALER_INT_ILLEGAL_SRC_YV_POS		8
#define SCALER_INT_ILLEGAL_SRC_YH_POS		7
#define SCALER_INT_ILLEGAL_SRC_C_SPAN		6
#define SCALER_INT_ILLEGAL_SRC_Y_SPAN		5
#define SCALER_INT_ILLEGAL_SRC_CR_BASE		4
#define SCALER_INT_ILLEGAL_SRC_CB_BASE		3
#define SCALER_INT_ILLEGAL_SRC_Y_BASE		2
#define SCALER_INT_ILLEGAL_SRC_COLOR		1
#define SCALER_INT_FRAME_END			0

/* SCALER interrupt enable */
#define SCALER_INT_EN				0x08
#define SCALER_INT_EN_DEFAULT			0x81ffffff

/* SCALER interrupt status */
#define SCALER_INT_STATUS			0x0c
#define SCALER_INT_STATUS_CLEAR			0xffffffff
#define SCALER_INT_STATUS_ERROR			0x81fffffe

/* SCALER source format configuration */
#define SCALER_SRC_CFG				0x10
#define SCALER_SRC_TILE_EN			(0x1 << 10)
#define SCALER_SRC_BYTE_SWAP_MASK		0x3
#define SCALER_SRC_BYTE_SWAP_SHIFT		5
#define SCALER_SRC_COLOR_FORMAT_MASK		0xf
#define SCALER_SRC_COLOR_FORMAT_SHIFT		0

/* SCALER source y-base */
#define SCALER_SRC_Y_BASE			0x14

/* SCALER source cb-base */
#define SCALER_SRC_CB_BASE			0x18

/* SCALER source cr-base */
#define SCALER_SRC_CR_BASE			0x294

/* SCALER source span */
#define SCALER_SRC_SPAN				0x1c
#define SCALER_SRC_C_SPAN_MASK			0x3fff
#define SCALER_SRC_C_SPAN_SHIFT			16
#define SCALER_SRC_Y_SPAN_MASK			0x3fff
#define SCALER_SRC_Y_SPAN_SHIFT			0

/*
 * SCALER source y-position
 * 14.2 fixed-point format
 *      - 14 bits at the MSB are for the integer part.
 *      - 2 bits at LSB are for fractional part and always has to be set to 0.
 */
#define SCALER_SRC_Y_POS			0x20
#define SCALER_SRC_YH_POS_MASK			0xfffc
#define SCALER_SRC_YH_POS_SHIFT			16
#define SCALER_SRC_YV_POS_MASK			0xfffc
#define SCALER_SRC_YV_POS_SHIFT			0

/* SCALER source width/height */
#define SCALER_SRC_WH				0x24
#define SCALER_SRC_WIDTH_MASK			0x3fff
#define SCALER_SRC_WIDTH_SHIFT			16
#define SCALER_SRC_HEIGHT_MASK			0x3fff
#define SCALER_SRC_HEIGHT_SHIFT			0

/*
 * SCALER source c-position
 * 14.2 fixed-point format
 *      - 14 bits at the MSB are for the integer part.
 *      - 2 bits at LSB are for fractional part and always has to be set to 0.
 */
#define SCALER_SRC_C_POS			0x28
#define SCALER_SRC_CH_POS_MASK			0xfffc
#define SCALER_SRC_CH_POS_SHIFT			16
#define SCALER_SRC_CV_POS_MASK			0xfffc
#define SCALER_SRC_CV_POS_SHIFT			0

/* SCALER destination format configuration */
#define SCALER_DST_CFG				0x30
#define SCALER_DST_BYTE_SWAP_MASK		0x3
#define SCALER_DST_BYTE_SWAP_SHIFT		5
#define SCALER_DST_COLOR_FORMAT_MASK		0xf

/* SCALER destination y-base */
#define SCALER_DST_Y_BASE			0x34

/* SCALER destination cb-base */
#define SCALER_DST_CB_BASE			0x38

/* SCALER destination cr-base */
#define SCALER_DST_CR_BASE			0x298

/* SCALER destination span */
#define SCALER_DST_SPAN				0x3c
#define SCALER_DST_C_SPAN_MASK			0x3fff
#define SCALER_DST_C_SPAN_SHIFT			16
#define SCALER_DST_Y_SPAN_MASK			0x3fff
#define SCALER_DST_Y_SPAN_SHIFT			0

/* SCALER destination width/height */
#define SCALER_DST_WH				0x40
#define SCALER_DST_WIDTH_MASK			0x3fff
#define SCALER_DST_WIDTH_SHIFT			16
#define SCALER_DST_HEIGHT_MASK			0x3fff
#define SCALER_DST_HEIGHT_SHIFT			0

/* SCALER destination position */
#define SCALER_DST_POS				0x44
#define SCALER_DST_H_POS_MASK			0x3fff
#define SCALER_DST_H_POS_SHIFT			16
#define SCALER_DST_V_POS_MASK			0x3fff
#define SCALER_DST_V_POS_SHIFT			0

/* SCALER horizontal scale ratio */
#define SCALER_H_RATIO				0x50
#define SCALER_H_RATIO_MASK			0x7ffff
#define SCALER_H_RATIO_SHIFT			0

/* SCALER vertical scale ratio */
#define SCALER_V_RATIO				0x54
#define SCALER_V_RATIO_MASK			0x7ffff
#define SCALER_V_RATIO_SHIFT			0

/* SCALER rotation config */
#define SCALER_ROT_CFG				0x58
#define SCALER_FLIP_X_EN			(1 << 3)
#define SCALER_FLIP_Y_EN			(1 << 2)
#define SCALER_ROTMODE_MASK			0x3
#define SCALER_ROTMODE_SHIFT			0

/* SCALER csc coefficients */
#define SCALER_CSC_COEF(x, y)			(0x220 + ((x * 12) + (y * 4)))

/* SCALER dither config */
#define SCALER_DITH_CFG				0x250
#define SCALER_DITHER_R_TYPE_MASK		0x7
#define SCALER_DITHER_R_TYPE_SHIFT		6
#define SCALER_DITHER_G_TYPE_MASK		0x7
#define SCALER_DITHER_G_TYPE_SHIFT		3
#define SCALER_DITHER_B_TYPE_MASK		0x7
#define SCALER_DITHER_B_TYPE_SHIFT		0

/* SCALER src blend color */
#define SCALER_SRC_BLEND_COLOR			0x280
#define SCALER_SRC_COLOR_SEL_INV		(1 << 31)
#define SCALER_SRC_COLOR_SEL_MASK		0x3
#define SCALER_SRC_COLOR_SEL_SHIFT		29
#define SCALER_SRC_COLOR_OP_SEL_INV		(1 << 28)
#define SCALER_SRC_COLOR_OP_SEL_MASK		0xf
#define SCALER_SRC_COLOR_OP_SEL_SHIFT		24
#define SCALER_SRC_GLOBAL_COLOR0_MASK		0xff
#define SCALER_SRC_GLOBAL_COLOR0_SHIFT		16
#define SCALER_SRC_GLOBAL_COLOR1_MASK		0xff
#define SCALER_SRC_GLOBAL_COLOR1_SHIFT		8
#define SCALER_SRC_GLOBAL_COLOR2_MASK		0xff
#define SCALER_SRC_GLOBAL_COLOR2_SHIFT		0

/* SCALER src blend alpha */
#define SCALER_SRC_BLEND_ALPHA			0x284
#define SCALER_SRC_ALPHA_SEL_INV		(1 << 31)
#define SCALER_SRC_ALPHA_SEL_MASK		0x3
#define SCALER_SRC_ALPHA_SEL_SHIFT		29
#define SCALER_SRC_ALPHA_OP_SEL_INV		(1 << 28)
#define SCALER_SRC_ALPHA_OP_SEL_MASK		0xf
#define SCALER_SRC_ALPHA_OP_SEL_SHIFT		24
#define SCALER_SRC_GLOBAL_ALPHA_MASK		0xff
#define SCALER_SRC_GLOBAL_ALPHA_SHIFT		0

/* SCALER dst blend color */
#define SCALER_DST_BLEND_COLOR			0x288
#define SCALER_DST_COLOR_SEL_INV		(1 << 31)
#define SCALER_DST_COLOR_SEL_MASK		0x3
#define SCALER_DST_COLOR_SEL_SHIFT		29
#define SCALER_DST_COLOR_OP_SEL_INV		(1 << 28)
#define SCALER_DST_COLOR_OP_SEL_MASK		0xf
#define SCALER_DST_COLOR_OP_SEL_SHIFT		24
#define SCALER_DST_GLOBAL_COLOR0_MASK		0xff
#define SCALER_DST_GLOBAL_COLOR0_SHIFT		16
#define SCALER_DST_GLOBAL_COLOR1_MASK		0xff
#define SCALER_DST_GLOBAL_COLOR1_SHIFT		8
#define SCALER_DST_GLOBAL_COLOR2_MASK		0xff
#define SCALER_DST_GLOBAL_COLOR2_SHIFT		0

/* SCALER dst blend alpha */
#define SCALER_DST_BLEND_ALPHA			0x28c
#define SCALER_DST_ALPHA_SEL_INV		(1 << 31)
#define SCALER_DST_ALPHA_SEL_MASK		0x3
#define SCALER_DST_ALPHA_SEL_SHIFT		29
#define SCALER_DST_ALPHA_OP_SEL_INV		(1 << 28)
#define SCALER_DST_ALPHA_OP_SEL_MASK		0xf
#define SCALER_DST_ALPHA_OP_SEL_SHIFT		24
#define SCALER_DST_GLOBAL_ALPHA_MASK		0xff
#define SCALER_DST_GLOBAL_ALPHA_SHIFT		0

/* SCALER fill color */
#define SCALER_FILL_COLOR			0x290
#define SCALER_FILL_ALPHA_MASK			0xff
#define SCALER_FILL_ALPHA_SHIFT			24
#define SCALER_FILL_COLOR0_MASK			0xff
#define SCALER_FILL_COLOR0_SHIFT		16
#define SCALER_FILL_COLOR1_MASK			0xff
#define SCALER_FILL_COLOR1_SHIFT		8
#define SCALER_FILL_COLOR2_MASK			0xff
#define SCALER_FILL_COLOR2_SHIFT		0

/* SCALER address queue config */
#define SCALER_ADDR_QUEUE_CONFIG		0x2a0
#define SCALER_ADDR_QUEUE_RST			0x1

/* Arbitrary R/W register and value to check if soft reset succeeded */
#define SCALER_CFG_SOFT_RESET_CHECK_REG		SCALER_SRC_CFG
#define SCALER_CFG_SOFT_RESET_CHECK_VAL		0x3

struct scaler_error {
	u32 irq_num;
	const char * const name;
};

static const struct scaler_error scaler_errors[] = {
	{SCALER_INT_TIMEOUT,			"Timeout"},
	{SCALER_INT_ILLEGAL_BLEND,		"Illegal Blend setting"},
	{SCALER_INT_ILLEGAL_RATIO,		"Illegal Scale ratio setting"},
	{SCALER_INT_ILLEGAL_DST_HEIGHT,		"Illegal Dst Height"},
	{SCALER_INT_ILLEGAL_DST_WIDTH,		"Illegal Dst Width"},
	{SCALER_INT_ILLEGAL_DST_V_POS,		"Illegal Dst V-Pos"},
	{SCALER_INT_ILLEGAL_DST_H_POS,		"Illegal Dst H-Pos"},
	{SCALER_INT_ILLEGAL_DST_C_SPAN,		"Illegal Dst C-Span"},
	{SCALER_INT_ILLEGAL_DST_Y_SPAN,		"Illegal Dst Y-span"},
	{SCALER_INT_ILLEGAL_DST_CR_BASE,	"Illegal Dst Cr-base"},
	{SCALER_INT_ILLEGAL_DST_CB_BASE,	"Illegal Dst Cb-base"},
	{SCALER_INT_ILLEGAL_DST_Y_BASE,		"Illegal Dst Y-base"},
	{SCALER_INT_ILLEGAL_DST_COLOR,		"Illegal Dst Color"},
	{SCALER_INT_ILLEGAL_SRC_HEIGHT,		"Illegal Src Height"},
	{SCALER_INT_ILLEGAL_SRC_WIDTH,		"Illegal Src Width"},
	{SCALER_INT_ILLEGAL_SRC_CV_POS,		"Illegal Src Chroma V-pos"},
	{SCALER_INT_ILLEGAL_SRC_CH_POS,		"Illegal Src Chroma H-pos"},
	{SCALER_INT_ILLEGAL_SRC_YV_POS,		"Illegal Src Luma V-pos"},
	{SCALER_INT_ILLEGAL_SRC_YH_POS,		"Illegal Src Luma H-pos"},
	{SCALER_INT_ILLEGAL_SRC_C_SPAN,		"Illegal Src C-span"},
	{SCALER_INT_ILLEGAL_SRC_Y_SPAN,		"Illegal Src Y-span"},
	{SCALER_INT_ILLEGAL_SRC_CR_BASE,	"Illegal Src Cr-base"},
	{SCALER_INT_ILLEGAL_SRC_CB_BASE,	"Illegal Src Cb-base"},
	{SCALER_INT_ILLEGAL_SRC_Y_BASE,		"Illegal Src Y-base"},
	{SCALER_INT_ILLEGAL_SRC_COLOR,		"Illegal Src Color setting"},
};

#define SCALER_NUM_ERRORS	ARRAY_SIZE(scaler_errors)

static inline u32 scaler_read(struct scaler_dev *dev, u32 offset)
{
	return readl(dev->regs + offset);
}

static inline void scaler_write(struct scaler_dev *dev, u32 offset, u32 value)
{
	writel(value, dev->regs + offset);
}

static inline void scaler_hw_address_queue_reset(struct scaler_ctx *ctx)
{
	scaler_write(ctx->scaler_dev, SCALER_ADDR_QUEUE_CONFIG,
					SCALER_ADDR_QUEUE_RST);
}

void scaler_hw_set_sw_reset(struct scaler_dev *dev);
int scaler_wait_reset(struct scaler_dev *dev);
void scaler_hw_set_irq(struct scaler_dev *dev, int interrupt, bool mask);
void scaler_hw_set_input_addr(struct scaler_dev *dev, struct scaler_addr *addr);
void scaler_hw_set_output_addr(struct scaler_dev *dev,
				struct scaler_addr *addr);
void scaler_hw_set_in_size(struct scaler_ctx *ctx);
void scaler_hw_set_in_image_format(struct scaler_ctx *ctx);
void scaler_hw_set_out_size(struct scaler_ctx *ctx);
void scaler_hw_set_out_image_format(struct scaler_ctx *ctx);
void scaler_hw_set_scaler_ratio(struct scaler_ctx *ctx);
void scaler_hw_set_rotation(struct scaler_ctx *ctx);
void scaler_hw_set_csc_coeff(struct scaler_ctx *ctx);
void scaler_hw_src_y_offset_en(struct scaler_dev *dev, bool on);
void scaler_hw_dst_y_offset_en(struct scaler_dev *dev, bool on);
void scaler_hw_enable_control(struct scaler_dev *dev, bool on);
unsigned int scaler_hw_get_irq_status(struct scaler_dev *dev);
void scaler_hw_clear_irq(struct scaler_dev *dev, unsigned int irq);

#endif /* REGS_SCALER_H_ */
