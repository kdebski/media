/*
 * Copyright (c) 2013 Samsung Electronics Co., Ltd.
 *		http://www.samsung.com
 *
 * Header file for Samsung EXYNOS5 SoC series SCALER driver
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#ifndef SCALER_CORE_H_
#define SCALER_CORE_H_

#include <linux/device.h>
#include <linux/platform_device.h>
#include <media/v4l2-ctrls.h>
#include <media/v4l2-device.h>
#include <media/v4l2-mem2mem.h>
#include <media/videobuf2-dma-contig.h>

#define scaler_dbg(_dev, fmt, args...) dev_dbg(&_dev->pdev->dev, fmt, ##args)

#define SCALER_MODULE_NAME		"exynos5-scaler"

#define SCALER_SHUTDOWN_TIMEOUT		((100 * HZ) / 1000)
#define SCALER_MAX_DEVS			4
#define SCALER_MAX_CTRL_NUM		10
#define SCALER_SC_ALIGN_4		4
#define SCALER_SC_ALIGN_2		2
#define DEFAULT_CSC_EQ			1
#define DEFAULT_CSC_RANGE		1
#define SCALER_MAX_PLANES		3

#define SCALER_PARAMS			(1 << 0)
#define SCALER_CTX_STOP_REQ		(1 << 1)

/* color format */
#define SCALER_RGB			(0x1 << 0)
#define SCALER_YUV420			(0x1 << 1)
#define SCALER_YUV422			(0x1 << 2)
#define SCALER_YUV444			(0x1 << 3)

/* yuv color order */
#define SCALER_CBCR			0
#define SCALER_CRCB			1

#define	SCALER_YUV420_2P_Y_UV		0
#define SCALER_YUV422_2P_Y_UV		2
#define SCALER_YUV444_2P_Y_UV		3
#define SCALER_RGB565			4
#define SCALER_ARGB1555			5
#define SCALER_ARGB8888			6
#define SCALER_PREMULTIPLIED_ARGB8888	7
#define SCALER_YUV422_1P_YVYU		9
#define SCALER_YUV422_1P_YUYV		10
#define SCALER_YUV422_1P_UYVY		11
#define SCALER_ARGB4444			12
#define SCALER_L8A8			13
#define SCALER_RGBA8888			14
#define SCALER_L8			15
#define SCALER_YUV420_2P_Y_VU		16
#define SCALER_YUV422_2P_Y_VU		18
#define SCALER_YUV444_2P_Y_VU		19
#define SCALER_YUV420_3P_Y_U_V		20
#define SCALER_YUV422_3P_Y_U_V		22
#define SCALER_YUV444_3P_Y_U_V		23

#define	SCALER_FMT_SRC			(0x1 << 0)
#define	SCALER_FMT_DST			(0x1 << 1)
#define	SCALER_FMT_TILED		(0x1 << 2)

enum scaler_dev_flags {
	/* for global */
	ST_SUSPEND,

	/* for m2m node */
	ST_M2M_OPEN,
	ST_M2M_RUN,
	ST_M2M_PEND,
	ST_M2M_SUSPENDED,
	ST_M2M_SUSPENDING,
};

#define fh_to_ctx(__fh)		container_of(__fh, struct scaler_ctx, fh)
#define is_rgb(fmt)		(!!(((fmt)->color) & SCALER_RGB))
#define is_yuv(fmt)		((fmt->color >= SCALER_YUV420) && \
					(fmt->color <= SCALER_YUV444))
#define is_yuv420(fmt)		(!!((fmt->color) & SCALER_YUV420))
#define is_yuv422(fmt)		(!!((fmt->color) & SCALER_YUV422))
#define is_yuv422_1p(fmt)	(is_yuv422(fmt) && (fmt->num_planes == 1))
#define is_yuv420_2p(fmt)	(is_yuv420(fmt) && (fmt->num_planes == 2))
#define is_yuv422_2p(fmt)	(is_yuv422(fmt) && (fmt->num_planes == 2))
#define is_yuv42x_2p(fmt)	(is_yuv420_2p(fmt) || is_yuv422_2p(fmt))
#define is_src_fmt(fmt)		((fmt->flags) & SCALER_FMT_SRC)
#define is_dst_fmt(fmt)		((fmt->flags) & SCALER_FMT_DST)
#define is_tiled_fmt(fmt)	((fmt->flags) & SCALER_FMT_TILED)

#define scaler_m2m_active(dev)	test_bit(ST_M2M_RUN, &(dev)->state)
#define scaler_m2m_pending(dev)	test_bit(ST_M2M_PEND, &(dev)->state)
#define scaler_m2m_opened(dev)	test_bit(ST_M2M_OPEN, &(dev)->state)

#define ctrl_to_ctx(__ctrl) \
	container_of((__ctrl)->handler, struct scaler_ctx, ctrl_handler)
/**
 * struct scaler_fmt - the driver's internal color format data
 * @scaler_color: SCALER color format
 * @name: format description
 * @pixelformat: the fourcc code for this format, 0 if not applicable
 * @color_order: Chrominance order control
 * @num_planes: number of physically non-contiguous data planes
 * @num_comp: number of physically contiguous data planes
 * @depth: per plane driver's private 'number of bits per pixel'
 * @flags: flags indicating which operation mode format applies to
 */
struct scaler_fmt {
	u32	scaler_color;
	char	*name;
	u32	pixelformat;
	u32	color;
	u32	color_order;
	u16	num_planes;
	u16	num_comp;
	u8	depth[SCALER_MAX_PLANES];
	u32	flags;
};

/**
 * struct scaler_input_buf - the driver's video buffer
 * @vb:	videobuf2 buffer
 * @list : linked list structure for buffer queue
 * @idx : index of SCALER input buffer
 */
struct scaler_input_buf {
	struct vb2_buffer	vb;
	struct list_head	list;
	int			idx;
};

/**
 * struct scaler_addr - the SCALER DMA address set
 * @y:	 luminance plane address
 * @cb:	 Cb plane address
 * @cr:	 Cr plane address
 */
struct scaler_addr {
	dma_addr_t y;
	dma_addr_t cb;
	dma_addr_t cr;
};

/**
 * struct scaler_ctrls - the SCALER control set
 * @rotate: rotation degree
 * @hflip: horizontal flip
 * @vflip: vertical flip
 * @global_alpha: the alpha value of current frame
 */
struct scaler_ctrls {
	struct v4l2_ctrl *rotate;
	struct v4l2_ctrl *hflip;
	struct v4l2_ctrl *vflip;
	struct v4l2_ctrl *global_alpha;
};

/* struct scaler_csc_info - color space conversion information */
enum scaler_csc_coeff {
	SCALER_CSC_COEFF_YCBCR_TO_RGB,
	SCALER_CSC_COEFF_RGB_TO_YCBCR,
	SCALER_CSC_COEFF_MAX,
	SCALER_CSC_COEFF_NONE,
};

struct scaler_csc_info {
	enum scaler_csc_coeff coeff_type;
};

/**
 * struct scaler_scaler - the configuration data for SCALER inetrnal scaler
 * @hratio:	the main scaler's horizontal ratio
 * @vratio:	the main scaler's vertical ratio
 */
struct scaler_scaler {
	u32 hratio;
	u32 vratio;
};

struct scaler_dev;
struct scaler_ctx;

/**
 * struct scaler_frame - source/target frame properties
 * @f_width:	SRC : SRCIMG_WIDTH, DST : OUTPUTDMA_WHOLE_IMG_WIDTH
 * @f_height:	SRC : SRCIMG_HEIGHT, DST : OUTPUTDMA_WHOLE_IMG_HEIGHT
 * @selection:	crop(source)/compose(destination) size
 * @payload:	image size in bytes (w x h x bpp)
 * @addr:	image frame buffer DMA addresses
 * @fmt:	SCALER color format pointer
 * @colorspace: value indicating v4l2_colorspace
 * @alpha:	frame's alpha value
 */
struct scaler_frame {
	u32 f_width;
	u32 f_height;
	struct v4l2_rect selection;
	unsigned long payload[SCALER_MAX_PLANES];
	struct scaler_addr	addr;
	const struct scaler_fmt *fmt;
	u32 colorspace;
	u8 alpha;
};

/**
 * struct scaler_m2m_device - v4l2 memory-to-memory device data
 * @vfd: the video device node for v4l2 m2m mode
 * @m2m_dev: v4l2 memory-to-memory device data
 * @ctx: hardware context data
 */
struct scaler_m2m_device {
	struct video_device	*vfd;
	struct v4l2_m2m_dev	*m2m_dev;
	struct scaler_ctx	*ctx;
};

/* struct scaler_pix_input - image pixel size limits for input frame */
struct scaler_frm_limit {
	u16	min_w;
	u16	min_h;
	u16	max_w;
	u16	max_h;

};

struct scaler_pix_align {
	u16 src_w_420;
	u16 src_w_422;
	u16 src_h_420;
	u16 dst_w_420;
	u16 dst_w_422;
	u16 dst_h_420;
};

/* struct scaler_variant - SCALER variant information */
struct scaler_variant {
	struct scaler_frm_limit	*pix_in;
	struct scaler_frm_limit	*pix_out;
	struct scaler_pix_align	*pix_align;
	u16	scl_up_max;
	u16	scl_down_max;
	u16	in_buf_cnt;
	u16	out_buf_cnt;
};

/**
 * struct scaler_dev - abstraction for SCALER entity
 * @slock: the spinlock protecting this data structure
 * @lock: the mutex protecting this data structure
 * @pdev: pointer to the SCALER platform device
 * @variant: the IP variant information
 * @id: SCALER device index (0..SCALER_MAX_DEVS)
 * @clock: clocks required for SCALER operation
 * @regs: the mapped hardware registers
 * @irq_queue: interrupt handler waitqueue
 * @m2m: memory-to-memory V4L2 device information
 * @state: flags used to synchronize m2m and capture mode operation
 * @alloc_ctx: videobuf2 memory allocator context
 * @vdev: video device for SCALER instance
 */
struct scaler_dev {
	spinlock_t			slock;
	struct mutex			lock;
	struct platform_device		*pdev;
	struct scaler_variant		*variant;
	struct clk			*clock;
	void __iomem			*regs;
	wait_queue_head_t		irq_queue;
	struct scaler_m2m_device	m2m;
	unsigned long			state;
	struct vb2_alloc_ctx		*alloc_ctx;
	struct video_device		vdev;
	enum scaler_csc_coeff		coeff_type;
};

/**
 * scaler_ctx - the device context data
 * @s_frame: source frame properties
 * @d_frame: destination frame properties
 * @scaler: image scaler properties
 * @flags: additional flags for image conversion
 * @state: flags to keep track of user configuration
 * @scaler_dev: the SCALER device this context applies to
 * @m2m_ctx: memory-to-memory device context
 * @fh: v4l2 file handle
 * @ctrl_handler: v4l2 controls handler
 * @ctrls_scaler: SCALER control set
 * @ctrls_rdy: true if the control handler is initialized
 */
struct scaler_ctx {
	struct scaler_frame	s_frame;
	struct scaler_frame	d_frame;
	struct scaler_scaler	scaler;
	u32			flags;
	u32			state;
	int			rotation;
	unsigned int		hflip:1;
	unsigned int		vflip:1;
	struct scaler_dev	*scaler_dev;
	struct v4l2_m2m_ctx	*m2m_ctx;
	struct v4l2_fh		fh;
	struct v4l2_ctrl_handler ctrl_handler;
	struct scaler_ctrls	ctrls_scaler;
	bool			ctrls_rdy;
};

static inline void scaler_ctx_state_lock_set(u32 state, struct scaler_ctx *ctx)
{
	unsigned long flags;

	spin_lock_irqsave(&ctx->scaler_dev->slock, flags);
	ctx->state |= state;
	spin_unlock_irqrestore(&ctx->scaler_dev->slock, flags);
}

static inline void scaler_ctx_state_lock_clear(u32 state,
					struct scaler_ctx *ctx)
{
	unsigned long flags;

	spin_lock_irqsave(&ctx->scaler_dev->slock, flags);
	ctx->state &= ~state;
	spin_unlock_irqrestore(&ctx->scaler_dev->slock, flags);
}

static inline bool scaler_ctx_state_is_set(u32 mask, struct scaler_ctx *ctx)
{
	unsigned long flags;
	bool ret;

	spin_lock_irqsave(&ctx->scaler_dev->slock, flags);
	ret = (ctx->state & mask) == mask;
	spin_unlock_irqrestore(&ctx->scaler_dev->slock, flags);
	return ret;
}

void scaler_set_prefbuf(struct scaler_dev *scaler, struct scaler_frame *frm);
int scaler_register_m2m_device(struct scaler_dev *scaler);
void scaler_unregister_m2m_device(struct scaler_dev *scaler);
void scaler_m2m_job_finish(struct scaler_ctx *ctx, int vb_state);
u32 get_plane_size(struct scaler_frame *fr, unsigned int plane);
const struct scaler_fmt *scaler_get_format(int index);
const struct scaler_fmt *scaler_find_fmt(u32 *pixelformat, int index);
struct scaler_frame *ctx_get_frame(struct scaler_ctx *ctx,
			enum v4l2_buf_type type);
int scaler_enum_fmt_mplane(struct v4l2_fmtdesc *f);
int scaler_try_fmt_mplane(struct scaler_ctx *ctx, struct v4l2_format *f);
void scaler_set_frame_size(struct scaler_frame *frame, int width, int height);
int scaler_g_fmt_mplane(struct scaler_ctx *ctx, struct v4l2_format *f);
void scaler_check_crop_change(u32 tmp_w, u32 tmp_h, u32 *w, u32 *h);
int scaler_g_crop(struct scaler_ctx *ctx, struct v4l2_crop *cr);
int scaler_try_crop(struct scaler_ctx *ctx, struct v4l2_crop *cr);
int scaler_cal_prescaler_ratio(struct scaler_variant *var, u32 src, u32 dst,
			u32 *ratio);
void scaler_get_prescaler_shfactor(u32 hratio, u32 vratio, u32 *sh);
void scaler_check_src_scale_info(struct scaler_variant *var,
			struct scaler_frame *s_frame,
			u32 *wratio, u32 tx, u32 ty, u32 *hratio);
int scaler_check_scaler_ratio(struct scaler_variant *var, int sw, int sh,
			int dw,	int dh, int rot);
int scaler_set_scaler_info(struct scaler_ctx *ctx);
int scaler_ctrls_create(struct scaler_ctx *ctx);
void scaler_ctrls_delete(struct scaler_ctx *ctx);
int scaler_prepare_addr(struct scaler_ctx *ctx, struct vb2_buffer *vb,
			struct scaler_frame *frame, struct scaler_addr *addr);

#endif /* SCALER_CORE_H_ */
