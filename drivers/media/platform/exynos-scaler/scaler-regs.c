/*
 * Copyright (c) 2013 Samsung Electronics Co., Ltd.
 *		http://www.samsung.com
 *
 * Samsung EXYNOS5 SoC series SCALER driver
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/delay.h>
#include <linux/platform_device.h>

#include "scaler-regs.h"

/* Scaler reset timeout in milliseconds */
#define SCALER_RESET_TIMEOUT	50

void scaler_hw_set_sw_reset(struct scaler_dev *dev)
{
	u32 cfg;

	cfg = scaler_read(dev, SCALER_CFG);
	cfg |= SCALER_CFG_SOFT_RESET;

	scaler_write(dev, SCALER_CFG, cfg);
}

int scaler_wait_reset(struct scaler_dev *dev)
{
	unsigned long end = jiffies + msecs_to_jiffies(SCALER_RESET_TIMEOUT);
	u32 cfg, reset_done = 0;

	while (time_before(jiffies, end)) {
		cfg = scaler_read(dev, SCALER_CFG);
		if (!(cfg & SCALER_CFG_SOFT_RESET)) {
			reset_done = 1;
			break;
		}
		usleep_range(10, 20);
	}

	/*
	 * Write any value to read/write register and read it back.
	 * If the written and read value matches, then the reset process is
	 * succeeded.
	 */
	while (reset_done) {

		/*
		 * TODO: need to define number of tries before returning
		 * -EBUSY to the caller
		 */

		scaler_write(dev, SCALER_CFG_SOFT_RESET_CHECK_REG,
				SCALER_CFG_SOFT_RESET_CHECK_VAL);
		if (SCALER_CFG_SOFT_RESET_CHECK_VAL ==
			scaler_read(dev, SCALER_CFG_SOFT_RESET_CHECK_REG))
			return 0;
	}

	return -EBUSY;
}

void scaler_hw_set_irq(struct scaler_dev *dev, int irq_num, bool enable)
{
	u32 cfg;

	if ((irq_num < SCALER_INT_FRAME_END) ||
	    (irq_num > SCALER_INT_TIMEOUT))
		return;

	cfg = scaler_read(dev, SCALER_INT_EN);
	if (enable)
		cfg |= (1 << irq_num);
	else
		cfg &= ~(1 << irq_num);
	scaler_write(dev, SCALER_INT_EN, cfg);
}

void scaler_hw_set_input_addr(struct scaler_dev *dev, struct scaler_addr *addr)
{
	scaler_dbg(dev, "src_buf: 0x%x, cb: 0x%x, cr: 0x%x",
				addr->y, addr->cb, addr->cr);
	scaler_write(dev, SCALER_SRC_Y_BASE, addr->y);
	scaler_write(dev, SCALER_SRC_CB_BASE, addr->cb);
	scaler_write(dev, SCALER_SRC_CR_BASE, addr->cr);
}

void scaler_hw_set_output_addr(struct scaler_dev *dev,
			     struct scaler_addr *addr)
{
	scaler_dbg(dev, "dst_buf: 0x%x, cb: 0x%x, cr: 0x%x",
			addr->y, addr->cb, addr->cr);
	scaler_write(dev, SCALER_DST_Y_BASE, addr->y);
	scaler_write(dev, SCALER_DST_CB_BASE, addr->cb);
	scaler_write(dev, SCALER_DST_CR_BASE, addr->cr);
}

void scaler_hw_set_in_size(struct scaler_ctx *ctx)
{
	struct scaler_dev *dev = ctx->scaler_dev;
	struct scaler_frame *frame = &ctx->s_frame;
	u32 cfg;

	/* set input pixel offset */
	cfg = (frame->selection.left & SCALER_SRC_YH_POS_MASK) <<
				  SCALER_SRC_YH_POS_SHIFT;
	cfg |= ((frame->selection.top & SCALER_SRC_YV_POS_MASK) <<
				   SCALER_SRC_YV_POS_SHIFT);
	scaler_write(dev, SCALER_SRC_Y_POS, cfg);

	/* TODO: calculate 'C' plane h/v offset using 'Y' plane h/v offset */

	/* Set input span */
	cfg = (frame->f_width & SCALER_SRC_Y_SPAN_MASK) <<
				SCALER_SRC_Y_SPAN_SHIFT;
	if (is_yuv420_2p(frame->fmt))
		cfg |= ((frame->f_width & SCALER_SRC_C_SPAN_MASK) <<
					  SCALER_SRC_C_SPAN_SHIFT);
	else /* TODO: Verify */
		cfg |= ((frame->f_width & SCALER_SRC_C_SPAN_MASK) <<
					  SCALER_SRC_C_SPAN_SHIFT);

	scaler_write(dev, SCALER_SRC_SPAN, cfg);

	/* Set input cropped size */
	cfg = (frame->selection.width & SCALER_SRC_WIDTH_MASK) <<
				   SCALER_SRC_WIDTH_SHIFT;
	cfg |= ((frame->selection.height & SCALER_SRC_HEIGHT_MASK) <<
				      SCALER_SRC_HEIGHT_SHIFT);
	scaler_write(dev, SCALER_SRC_WH, cfg);

	scaler_dbg(dev, "src: posx: %d, posY: %d, spanY: %d, spanC: %d, cropX: %d, cropY: %d\n",
		frame->selection.left, frame->selection.top,
		frame->f_width, frame->f_width, frame->selection.width,
		frame->selection.height);
}

void scaler_hw_set_in_image_format(struct scaler_ctx *ctx)
{
	struct scaler_dev *dev = ctx->scaler_dev;
	struct scaler_frame *frame = &ctx->s_frame;
	u32 cfg;

	cfg = scaler_read(dev, SCALER_SRC_CFG);
	cfg &= ~(SCALER_SRC_COLOR_FORMAT_MASK << SCALER_SRC_COLOR_FORMAT_SHIFT);
	cfg |= ((frame->fmt->scaler_color & SCALER_SRC_COLOR_FORMAT_MASK) <<
					   SCALER_SRC_COLOR_FORMAT_SHIFT);

	/* Setting tiled/linear format */
	if (is_tiled_fmt(frame->fmt))
		cfg |= SCALER_SRC_TILE_EN;
	else
		cfg &= ~SCALER_SRC_TILE_EN;

	scaler_write(dev, SCALER_SRC_CFG, cfg);
}

void scaler_hw_set_out_size(struct scaler_ctx *ctx)
{
	struct scaler_dev *dev = ctx->scaler_dev;
	struct scaler_frame *frame = &ctx->d_frame;
	u32 cfg;

	/* Set output pixel offset */
	cfg = (frame->selection.left & SCALER_DST_H_POS_MASK) <<
				  SCALER_DST_H_POS_SHIFT;
	cfg |= (frame->selection.top & SCALER_DST_V_POS_MASK) <<
				  SCALER_DST_V_POS_SHIFT;
	scaler_write(dev, SCALER_DST_POS, cfg);

	/* Set output span */
	cfg = (frame->f_width & SCALER_DST_Y_SPAN_MASK) <<
				SCALER_DST_Y_SPAN_SHIFT;
	if (is_yuv420_2p(frame->fmt))
		cfg |= (((frame->f_width / 2) & SCALER_DST_C_SPAN_MASK) <<
					     SCALER_DST_C_SPAN_SHIFT);
	else
		cfg |= (((frame->f_width) & SCALER_DST_C_SPAN_MASK) <<
					     SCALER_DST_C_SPAN_SHIFT);
	scaler_write(dev, SCALER_DST_SPAN, cfg);

	/* Set output scaled size */
	cfg = (frame->selection.width & SCALER_DST_WIDTH_MASK) <<
				   SCALER_DST_WIDTH_SHIFT;
	cfg |= (frame->selection.height & SCALER_DST_HEIGHT_MASK) <<
				     SCALER_DST_HEIGHT_SHIFT;
	scaler_write(dev, SCALER_DST_WH, cfg);

	scaler_dbg(dev, "dst: pos X: %d, pos Y: %d, span Y: %d, span C: %d, crop X: %d, crop Y: %d\n",
		frame->selection.left, frame->selection.top,
		frame->f_width, frame->f_width, frame->selection.width,
		frame->selection.height);
}

void scaler_hw_set_out_image_format(struct scaler_ctx *ctx)
{
	struct scaler_dev *dev = ctx->scaler_dev;
	struct scaler_frame *frame = &ctx->d_frame;
	u32 cfg;

	cfg = scaler_read(dev, SCALER_DST_CFG);
	cfg &= ~SCALER_DST_COLOR_FORMAT_MASK;
	cfg |= (frame->fmt->scaler_color & SCALER_DST_COLOR_FORMAT_MASK);

	scaler_write(dev, SCALER_DST_CFG, cfg);
}

void scaler_hw_set_scaler_ratio(struct scaler_ctx *ctx)
{
	struct scaler_dev *dev = ctx->scaler_dev;
	struct scaler_scaler *sc = &ctx->scaler;
	u32 cfg;

	cfg = (sc->hratio & SCALER_H_RATIO_MASK) << SCALER_H_RATIO_SHIFT;
	scaler_write(dev, SCALER_H_RATIO, cfg);

	cfg = (sc->vratio & SCALER_V_RATIO_MASK) << SCALER_V_RATIO_SHIFT;
	scaler_write(dev, SCALER_V_RATIO, cfg);
}

void scaler_hw_set_rotation(struct scaler_ctx *ctx)
{
	struct scaler_dev *dev = ctx->scaler_dev;
	u32 cfg = 0;

	cfg = ((ctx->ctrls_scaler.rotate->val / 90) & SCALER_ROTMODE_MASK) <<
						      SCALER_ROTMODE_SHIFT;

	if (ctx->ctrls_scaler.hflip->val)
		cfg |= SCALER_FLIP_X_EN;

	if (ctx->ctrls_scaler.vflip->val)
		cfg |= SCALER_FLIP_Y_EN;

	scaler_write(dev, SCALER_ROT_CFG, cfg);
}

void scaler_hw_set_csc_coeff(struct scaler_ctx *ctx)
{
	struct scaler_dev *dev = ctx->scaler_dev;
	enum scaler_csc_coeff type;
	u32 cfg = 0;
	int i, j;
	static const u32 csc_coeff[SCALER_CSC_COEFF_MAX][3][3] = {
		{ /* YCbCr to RGB */
			{0x254, 0x000, 0x331},
			{0x254, 0xec8, 0xFA0},
			{0x254, 0x409, 0x000}
		},
		{ /* RGB to YCbCr */
			{0x084, 0x102, 0x032},
			{0xe4c, 0xe95, 0x0e1},
			{0x0e1, 0xebc, 0xe24}
		} };

	/* TODO: add check for BT.601,BT.709 narrow/wide ranges */
	if (is_rgb(ctx->s_frame.fmt) == is_rgb(ctx->d_frame.fmt)) {
		type = SCALER_CSC_COEFF_NONE;
	} else if (is_rgb(ctx->d_frame.fmt)) {
		type = SCALER_CSC_COEFF_YCBCR_TO_RGB;
		scaler_hw_src_y_offset_en(ctx->scaler_dev, true);
	} else {
		type = SCALER_CSC_COEFF_RGB_TO_YCBCR;
		scaler_hw_src_y_offset_en(ctx->scaler_dev, true);
	}

	if (type == ctx->scaler_dev->coeff_type || type >= SCALER_CSC_COEFF_MAX)
		return;

	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
			cfg = csc_coeff[type][i][j];
			scaler_write(dev, SCALER_CSC_COEF(i, j), cfg);
		}
	}

	ctx->scaler_dev->coeff_type = type;
}

void scaler_hw_src_y_offset_en(struct scaler_dev *dev, bool on)
{
	u32 cfg;

	cfg = scaler_read(dev, SCALER_CFG);
	if (on)
		cfg |= SCALER_CFG_CSC_Y_OFFSET_SRC_EN;
	else
		cfg &= ~SCALER_CFG_CSC_Y_OFFSET_SRC_EN;

	scaler_write(dev, SCALER_CFG, cfg);
}

void scaler_hw_dst_y_offset_en(struct scaler_dev *dev, bool on)
{
	u32 cfg;

	cfg = scaler_read(dev, SCALER_CFG);
	if (on)
		cfg |= SCALER_CFG_CSC_Y_OFFSET_DST_EN;
	else
		cfg &= ~SCALER_CFG_CSC_Y_OFFSET_DST_EN;

	scaler_write(dev, SCALER_CFG, cfg);
}

void scaler_hw_enable_control(struct scaler_dev *dev, bool on)
{
	u32 cfg;

	if (on)
		scaler_write(dev, SCALER_INT_EN, 0xffffffff);

	cfg = scaler_read(dev, SCALER_CFG);
	cfg |= SCALER_CFG_16_BURST_MODE;
	if (on)
		cfg |= SCALER_CFG_START_CMD;
	else
		cfg &= ~SCALER_CFG_START_CMD;

	scaler_dbg(dev, "%s: SCALER_CFG:0x%x\n", __func__, cfg);

	scaler_write(dev, SCALER_CFG, cfg);
}

unsigned int scaler_hw_get_irq_status(struct scaler_dev *dev)
{
	return scaler_read(dev, SCALER_INT_STATUS);
}

void scaler_hw_clear_irq(struct scaler_dev *dev, unsigned int irq)
{
	scaler_write(dev, SCALER_INT_STATUS, irq);
}
